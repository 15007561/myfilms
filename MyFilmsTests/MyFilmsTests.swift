//
//  MyFilmsTests.swift
//  MyFilmsTests
//
//  Created by admin on 02/11/2017.
//  Copyright © 2017 15007561. All rights reserved.
//

import XCTest
@testable import MyFilms

class MyFilmsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testFilmInitializationSucceeds() {
        // Zero rating
        let zeroRatingFilm = Film.init(id: 1, title: "Zero", photo: nil, rating: 0, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNotNil(zeroRatingFilm)
        
        // Highest positive rating
        let positiveRatingFilm = Film.init(id: 1, title: "Highest", photo: nil, rating: 5, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNotNil(positiveRatingFilm)
        
        // Id must be greater than zero
        let idGreaterThanZero = Film.init(id: 1, title: "Highest", photo: nil, rating: 5, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNotNil(idGreaterThanZero)
    }
    
    func testFilmInitializationFails() {
        // Negative rating
        let negativeRatingFilm = Film.init(id: 1, title: "Negative", photo: nil, rating: -1, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNil(negativeRatingFilm)
        
        // Rating exceeds maximum
        let largeRatingFilm = Film.init(id: 1, title: "Zero", photo: nil, rating: 6, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNil(largeRatingFilm)
        
        // Empty title String
        let emptyStringFilm = Film.init(id: 1, title: "", photo: nil, rating: 0, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNil(emptyStringFilm)
        
        // ID is equal to 0
        let idIsZero = Film.init(id: 0, title: "Highest", photo: nil, rating: 5, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNil(idIsZero)
        
        // ID is less than 0
        let idLessThanZero = Film.init(id: -1, title: "Highest", photo: nil, rating: 5, year: "1900", ageRating: "U", genre: "Action")
        XCTAssertNil(idLessThanZero)
    }
}
