//
//  FilmUITableViewController.swift
//  MyFilms
//
//  Created by Hertzsch D H (FCES) on 15/11/2017.
//  Copyright © 2017 15007561. All rights reserved.
//
import os.log
import UIKit

class FilmTableViewController: UITableViewController, UITextFieldDelegate {
    //MARK: Properties
    var films = [Film]()
    var filteredFilms = [Film]()
    
    // sort variables
    var sortTitleAtoZ: Bool = true
    var sortYearLowHigh: Bool = true
    var sortRatingHighLow: Bool = true
    
    // sort buttons
    @IBOutlet weak var buttonTitle: UIButton!
    @IBOutlet weak var buttonYear: UIButton!
    @IBOutlet weak var buttonRating: UIButton!
    
    @IBOutlet var tableViewFilms: UITableView!
    @IBOutlet weak var textFieldSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem
        
        // load saved films
        if let savedFilms = loadFilms() {
            films += savedFilms
        } else {
            // load sample data
            loadSampleFilms()
        }
        // set filtered films
        filteredFilms = films
        
        // text field delegate
        textFieldSearch.delegate = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredFilms.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "FilmTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FilmTableViewCell else {
            fatalError("The dequeued cell is not an instance of FilmTableViewCell")
        }
        
        // get the appropriate film
        let film = filteredFilms[indexPath.row]
        // set cell controls with film data
        cell.labelTitle.text = formatTitleYear(title: film.title, year: film.year)
        cell.imageViewPhoto.image = film.photo
        cell.ratingControl.rating = film.rating
        cell.imageViewAgeRating.image = film.getAgeRatingImage()
        cell.lableGenre.text = film.genre
        cell.buttonComments.tag = film.id
        // set accessibility properties
        cell.accessibilityLabel = "Film"
        cell.accessibilityHint = "Tap to edit film properties."
        cell.accessibilityValue = "\(film.title) (\(film.year)) \(film.genre)"
        cell.imageViewAgeRating.accessibilityValue = film.ageRating;
        cell.imageViewAgeRating.accessibilityLabel = "Age Rating"
        cell.imageViewAgeRating.accessibilityHint = "Show the age rating of the film."
        
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from films and filteredFilms using ID
            let index = getFilmIndex(id: filteredFilms[indexPath.row].id)
            films.remove(at: index)
            filteredFilms.remove(at: indexPath.row)
            saveFilms()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? "") {
        case "AddItem":
            os_log("Adding a new film.", log: OSLog.default, type: .debug)
        case "ShowDetail":
            guard let filmDetailViewController = segue.destination as? FilmViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedFilmCell = sender as? FilmTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedFilmCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedFilm = filteredFilms[indexPath.row]
            filmDetailViewController.film = selectedFilm
        case "Comments":
            guard let commentViewControl = segue.destination as? CommentsTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedFilmButton = sender as? UIButton else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            let selectedFilm = films[getFilmIndex(id: selectedFilmButton.tag)]
            commentViewControl.film = selectedFilm
        default:
            fatalError("Unexpectd seque identifier: \(String(describing: segue.identifier))")
        }
    }
    
    // MARK: Actions
    @IBAction func unwindToFilmList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? FilmViewController, let film = sourceViewController.film {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // update both films and filmsFiltered
                let index = getFilmIndex(id: filteredFilms[selectedIndexPath.row].id)
                films[index] = film
                filteredFilms[selectedIndexPath.row] = film
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else {
            // add new film to films and filteredFilms
            films.append(film)
            filteredFilms.append(film)
            updateData()
            }
            saveFilms()
        }
    }
    
    @IBAction func updateFilmList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? CommentsTableViewController, let film = sourceViewController.film {
            let filmIndex = getFilmIndex(id: film.id)
            films[filmIndex].comments = film.comments
            saveFilms()
        }
    }
    
    @IBAction func sortBy(_ sender: UIButton) {
        switch sender {
            case buttonTitle:
                if sortTitleAtoZ {
                    filteredFilms = filteredFilms.sorted{$0.title.lowercased() < $1.title.lowercased()}
                    sortTitleAtoZ = false
                } else {
                    filteredFilms = filteredFilms.sorted{$0.title.lowercased() > $1.title.lowercased()}
                    sortTitleAtoZ = true
                }
                sortYearLowHigh = true
                sortRatingHighLow = true
            case buttonYear:
                if sortYearLowHigh {
                    filteredFilms = filteredFilms.sorted{$0.year < $1.year}
                    sortYearLowHigh = false
                } else {
                    filteredFilms = filteredFilms.sorted{$0.year > $1.year}
                    sortYearLowHigh = true
                }
                sortTitleAtoZ = true
                sortRatingHighLow = true
            case buttonRating:
                if sortRatingHighLow {
                    filteredFilms = filteredFilms.sorted{$0.rating > $1.rating}
                    sortRatingHighLow = false
                } else {
                    filteredFilms = filteredFilms.sorted{$0.rating < $1.rating}
                    sortRatingHighLow = true
                }
                sortTitleAtoZ = true
                sortYearLowHigh = true
            default:
                break
        }
        updateData()
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // hide the keyboard
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    @IBAction func searchFilms(_ sender: UITextField) {
        // if not searching show all films
        // other search and show results
        if sender.text == "" {
            filteredFilms = films
        } else {
            filteredFilms = films.filter{$0.title.lowercased().contains(sender.text!.lowercased())}
        }
        updateData()
    }
    
    //MARK: Private methods
    private func loadSampleFilms() {
        let photo1 = UIImage(named: "film1")
        let photo2 = UIImage(named: "film2")
        let photo3 = UIImage(named: "film3")
        let film1Comments: [String] = ["This is so scary!","Slimer!"]
        let film2Comments: [String] = ["First!","Why does that guy wear a helmet? Is it a fashion statement?","Where does one buy a lightsaber?"]
        
        guard let film1 = Film(id: Film.getID(), title: "Ghostbusters", photo: photo1, rating: 3, year: "1984", ageRating: Film.AgeRating.rPG.rawValue, genre: "Action", comments: film1Comments) else {
        fatalError("Unable to instantiate film1")
        }
        
        guard let film2 = Film(id: Film.getID(), title: "Star Wars", photo: photo2, rating: 2, year: "1977", ageRating: Film.AgeRating.rU.rawValue, genre: "Fantasy", comments: film2Comments) else {
        fatalError("Unable to instantiate film2")
        }
        
        guard let film3 = Film(id: Film.getID(), title: "Back to the Future", photo: photo3, rating: 4, year: "1985", ageRating: Film.AgeRating.rPG.rawValue, genre: "Sci-Fi", comments: nil) else {
        fatalError("Unable to instantiate film3")
        }
        
        films += [film1, film2, film3]
    }
    
    private func saveFilms() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(films, toFile: Film.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Films successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save films!", log: OSLog.default, type: .debug)
        }
    }
    
    private func loadFilms() -> [Film]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Film.ArchiveURL.path) as? [Film]
    }
    
    private func updateData() {
        self.tableViewFilms.reloadData()
    }
    
    // Will find the film in films by id
    // and return the index
    // returns -1 if not found
    private func getFilmIndex(id: Int) -> Int {
        var index = 0
        while index < films.count {
            if films[index].id == id {
                return index
            }
            index += 1
        }
        return -1
    }
    
    // Checks the length of the title and will trim it
    // if it is too long. Then appends the year to the string.
    private func formatTitleYear(title: String, year: String) -> String {
        let MAX = 20
        var titleYear: String
        
        if title == "" {
            return "No Film Title (\(year)"
        }
        
        if title.count > MAX {
            let index = title.index(title.startIndex, offsetBy: MAX)
            titleYear = String(title[...index])
            titleYear += " (\(year))"
        } else {
            titleYear = "\(title) (\(year))"
        }
        
        return titleYear
    }
}
