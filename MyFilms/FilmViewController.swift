//
//  FilmViewController.swift
//  MyFilms
//
//  Created by admin on 02/11/2017.
//  Copyright © 2017 15007561. All rights reserved.
//
import os.log
import UIKit

class FilmViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: Properties
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textFieldYear: UITextField!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var buttonSave: UIBarButtonItem!
    @IBOutlet weak var textFieldAgeRating: UITextField!
    @IBOutlet weak var textFieldGenre: UITextField!
    var id: Int = 0
    var filmComments: [String]?
    
    // the current film either to edit or new
    var film: Film?
    
    // age rating picker
    var pickerViewAgeRating = UIPickerView()
    var pickerDataAgeRating:[String] = [String]()
    
    // genre picker
    var pickerViewGenre = UIPickerView()
    var pickerDataGenre:[String] = [String]()
    
    // year picker
    var pickerViewYear = UIPickerView()
    var pickerDataYear:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // manage input through delegate callbacks
        textFieldTitle.delegate = self
        textFieldYear.delegate = self
        textFieldAgeRating.delegate = self
        textFieldGenre.delegate = self
        
        // age rating picker view
        pickerViewAgeRating.delegate = self
        pickerViewAgeRating.dataSource = self
        pickerDataAgeRating = ["U","PG","12","12A","15","18","R18"]
        textFieldAgeRating.inputView = pickerViewAgeRating
        
        // genre picker
        pickerViewGenre.delegate = self
        pickerViewGenre.dataSource = self
        pickerDataGenre = ["Action", "Animaiton", "Comedy", "Drama", "Fantasy", "Horror", "Romance", "Sci-Fi", "Thriller"]
        textFieldGenre.inputView = pickerViewGenre
        
        // year picker
        pickerViewYear.delegate = self
        pickerViewGenre.dataSource = self
        pickerDataYear = getYearPickerData()
        textFieldYear.inputView = pickerViewYear
        
        // display film info if editing
        // otherwise get an ID for a new film
        if let film = film {
            id = film.id
            navigationItem.title = film.title
            textFieldTitle.text = film.title
            textFieldYear.text = film.year
            imageViewPhoto.image = film.photo
            ratingControl.rating = film.rating
            textFieldAgeRating.text = film.ageRating
            textFieldGenre.text = film.genre
            filmComments = film.comments
        } else {
            id = Film.getID()
            textFieldYear.text = pickerDataYear[0]
            textFieldAgeRating.text = pickerDataAgeRating[0]
            textFieldGenre.text = pickerDataGenre[0]
        }
        
        // Enable save button when the film title is not empty
        updateSaveButtonState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Picker Views
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case pickerViewGenre:
            return pickerDataGenre.count
        case pickerViewAgeRating:
            return pickerDataAgeRating.count
        case pickerViewYear:
            return pickerDataYear.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pickerViewGenre:
            return pickerDataGenre[row]
        case pickerViewAgeRating:
            return pickerDataAgeRating[row]
        case pickerViewYear:
            return pickerDataYear[row]
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case pickerViewGenre:
            textFieldGenre.text = pickerDataGenre[row]
        case pickerViewAgeRating:
            textFieldAgeRating.text = pickerDataAgeRating[row]
        case pickerViewYear:
            textFieldYear.text = pickerDataYear[row]
        default:
            break
        }
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // move to the next text field
        if textField == textFieldTitle {
            textFieldYear.becomeFirstResponder()
        } else {
            // hide the keyboard
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case textFieldTitle:
            // disable the Save button while editing
            buttonSave.isEnabled = false
        case textFieldYear:
            // Set the text to the first item of picker data
            // for the three pickers below
            if textFieldYear.text == "" {
                textFieldYear.text = pickerDataYear[0]
            }
        case textFieldAgeRating:
            if textFieldAgeRating.text == "" {
                textFieldAgeRating.text = pickerDataAgeRating[0]
            }
        case textFieldGenre:
            if textFieldGenre.text == "" {
                textFieldGenre.text = pickerDataGenre[0]
            }
        default:
            break
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // check if the year is a valid
        if textField == textFieldYear {
            var isValidYear: Bool {
                do {
                    let regex = try NSRegularExpression(pattern: "^(19|20)\\d\\d", options: .caseInsensitive)
                    return regex.firstMatch(in: textFieldYear.text!, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, textFieldYear.text!.characters.count)) != nil
                } catch {
                    return false
                }
            }
            
            if isValidYear == false {
                let alertController = UIAlertController(title: "Film Year", message: "Please enter a valid year between 1900-2099.", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                self.present(alertController, animated: true, completion: nil)
                textFieldYear.text = pickerDataYear[0]
            }
            
            return isValidYear
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        if textField == textFieldTitle {
            navigationItem.title = textField.text
        }
    }

    // MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containinng an image, but was provided the following: \(info)")
        }
        imageViewPhoto.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Navigation
    @IBAction func barButtonCancel(_ sender: UIBarButtonItem) {
        // dismiss depending on the style of presentation (modal or push)
        let isPresentingInAddFilmMode = presentingViewController is UINavigationController
        
        if isPresentingInAddFilmMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The FilmViewController is not inside a navigation controller.")
        }
    }
    
    // Configure a view controller before it's presented
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        guard let button = sender as? UIBarButtonItem, button == buttonSave else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        let id = self.id
        let title = textFieldTitle.text ?? ""
        let photo = imageViewPhoto.image
        let rating = ratingControl.rating
        let year = textFieldYear.text ?? "1900"
        let ageRating = textFieldAgeRating.text ?? "U"
        let genre = textFieldGenre.text ?? ""
        
        // set the film to be passed to the FilmTableViewController after the unwind seque
        film = Film(id: id, title: title, photo: photo, rating: rating, year: year, ageRating: ageRating, genre: genre, comments: filmComments)
    }
    
    // MARK: Actions
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer) {
        // hide the keyboard
        textFieldTitle.resignFirstResponder()
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    // MARK: Private methods
    private func updateSaveButtonState() {
        // Disable the Save button if the text field is empty.
        let text = textFieldTitle.text ?? ""
        buttonSave.isEnabled = !text.isEmpty
    }
    
    // Builds a list of years going backwards from the current year
    // This will be used by the year picker
    private func getYearPickerData() -> [String] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        let currentYear = dateFormatter.string(from: Date())
        var years: [String] = [String]()
        
        var year: Int = Int(currentYear)!
        while year > 1899 {
            years.append(String(year))
            year = year - 1
        }

        return years
    }
}

