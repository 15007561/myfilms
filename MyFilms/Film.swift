//
//  Film.swift
//  MyFilms
//
//  Created by Hertzsch D H (FCES) on 15/11/2017.
//  Copyright © 2017 15007561. All rights reserved.
//
import os.log
import UIKit

class Film: NSObject, NSCoding {
    // MARK: Properties
    var id: Int
    var title: String
    var photo: UIImage?
    var rating: Int
    var year: String
    var ageRating: String
    var genre: String
    var comments: [String]?
    
    // age rating enum
    enum AgeRating: String {
        case rU = "U"
        case rPG = "PG"
        case r12 = "12"
        case r12A = "12A"
        case r15 = "15"
        case r18 = "18"
        case rR18 = "R18"
    }
    
    // MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("films")
    
    // MARK: Types for NSCoding
    struct PropertyKey {
        static let id = "id"
        static let title = "title"
        static let photo = "photo"
        static let rating = "rating"
        static let year = "year"
        static let ageRating = "ageRating"
        static let genre = "genre"
        static let comments = "comments"
    }

    //MARK: Initialization
    init?(id: Int, title: String, photo: UIImage?, rating: Int, year: String, ageRating: String, genre: String, comments: [String]?) {
        // The title must not be empty
        guard !title.isEmpty else {
            return nil
        }
        
        // The rating must be between 0 and 5 inclusively
        guard (rating >= 0) && (rating <= 5) else {
            return nil
        }
        
        // the id must be greater than zero
        guard (id > 0) else {
            return nil
        }
        
        self.id = id
        self.title = title
        self.photo = photo
        self.rating = rating
        self.year = year
        self.ageRating = ageRating
        self.genre = genre
        self.comments = comments
    }
    
    // MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: PropertyKey.id)
        aCoder.encode(title, forKey: PropertyKey.title)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(rating, forKey: PropertyKey.rating)
        aCoder.encode(year, forKey: PropertyKey.year)
        aCoder.encode(ageRating, forKey: PropertyKey.ageRating)
        aCoder.encode(genre, forKey: PropertyKey.genre)
        aCoder.encode(comments, forKey: PropertyKey.comments)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // fail if unsable to decode
        let id = Int(aDecoder.decodeInteger(forKey: PropertyKey.id))
        guard let title = aDecoder.decodeObject(forKey: PropertyKey.title) as? String else {
            os_log("Unable to decode the title for a Film object.", log: OSLog.default, type: .debug)
            return nil
        }
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        let rating = aDecoder.decodeInteger(forKey: PropertyKey.rating)
        guard let year = aDecoder.decodeObject(forKey: PropertyKey.year) as? String else { // change to an Int
            os_log("Unable to decode the year for a Film object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let ageRating = aDecoder.decodeObject(forKey: PropertyKey.ageRating) as? String else {
            os_log("Unable to decode the age rating for a Film object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let genre = aDecoder.decodeObject(forKey: PropertyKey.genre) as? String else {
            os_log("Unable to decode the genre for a Film object.", log: OSLog.default, type: .debug)
            return nil
        }
        let comments = aDecoder.decodeObject(forKey: PropertyKey.comments) as? [String]

        // Must call designated initializer.
        self.init(id: id, title: title, photo: photo, rating: rating, year: year, ageRating: ageRating, genre: genre, comments: comments)
    }
    
    // Get the age rating image that corresponds
    // to the value given: U = image for U
    func getAgeRatingImage() -> UIImage {
        switch ageRating {
        case AgeRating.rU.rawValue:
            return UIImage(named: "rating_U")!
        case AgeRating.rPG.rawValue:
            return UIImage(named: "rating_PG")!
        case AgeRating.r12.rawValue:
            return UIImage(named: "rating_12")!
        case AgeRating.r12A.rawValue:
            return UIImage(named: "rating_12A")!
        case AgeRating.r15.rawValue:
            return UIImage(named: "rating_15")!
        case AgeRating.r18.rawValue:
            return UIImage(named: "rating_18")!
        case AgeRating.rR18.rawValue:
            return UIImage(named: "rating_R18")!
        default:
            ageRating = AgeRating.rU.rawValue
            return UIImage(named: "rating_U")!
        }
    }
    
    static func getID() -> Int {
        return Int(arc4random_uniform(UINT32_MAX))
    }
}
