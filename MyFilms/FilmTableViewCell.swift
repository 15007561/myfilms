//
//  FilmTableViewCell.swift
//  MyFilms
//
//  Created by Hertzsch D H (FCES) on 15/11/2017.
//  Copyright © 2017 15007561. All rights reserved.
//

import UIKit

class FilmTableViewCell: UITableViewCell {
    // MARK: Properties
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var imageViewAgeRating: UIImageView!
    @IBOutlet weak var lableGenre: UILabel!
    @IBOutlet weak var buttonComments: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
