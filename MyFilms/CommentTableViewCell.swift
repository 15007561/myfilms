//
//  CommentTableViewCell.swift
//  MyFilms
//
//  Created by Hertzsch D H (FCES) on 06/12/2017.
//  Copyright © 2017 15007561. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var labelComment: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
